<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <style>
        form{
            display: inline;
        }
        form button {
            background: transparent;
            padding: 0px;
            border: 0px;
        }
        form button i{
            color: #337AB7;
        }
    </style>
</head>
<body>
    @yield('content')
</body>
</html>